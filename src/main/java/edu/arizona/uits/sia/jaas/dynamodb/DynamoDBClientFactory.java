package edu.arizona.uits.sia.jaas.dynamodb;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.PredefinedClientConfigurations;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gary on 2/14/17.
 */
public class DynamoDBClientFactory {

    /**
     * Object CACHE.
     */
    private static final Map<String, AmazonDynamoDB> CACHE = new HashMap<>();

    /**
     * Cache ID option used on the JAAS config.
     */
    public static final String CACHE_ID = "cacheId";

    /**
     * Logger for this class.
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());


    /**
     * Return an AmazonDynamoDBClientBuilder based on config passed to JAAS module
     * @param jaasOptions
     * @return com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
     */
    private AmazonDynamoDBClientBuilder createBuilder(final Map<String, ?> jaasOptions) {
        // region
        String regionName = "us-west-2";
        // credentials
        AWSCredentialsProvider credsProvider = DefaultAWSCredentialsProviderChain.getInstance();
        // client configuration
        ClientConfiguration clientConfig = PredefinedClientConfigurations.dynamoDefault();

        // override default region if passed in options
        if (jaasOptions.containsKey("region")) {
            Region region = RegionUtils.getRegion((String)jaasOptions.get("region"));
            if (region != null) {
                regionName = region.getName();
            }
        }

        // build credential provider for explicit credentials
        if (jaasOptions.containsKey("accessKeyId") && jaasOptions.containsKey("secretAccessKey")) {
            credsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials((String)jaasOptions.get("accessKeyId"), (String)jaasOptions.get("secretAccessKey")));
        }

        // override max connections if passed in options
        if (jaasOptions.containsKey("maxConns")) {
            clientConfig.setMaxConnections(Integer.parseInt((String)jaasOptions.get("maxConns")));
        }

        return AmazonDynamoDBClientBuilder.standard()
                .withRegion(regionName)
                .withCredentials(credsProvider)
                .withClientConfiguration(clientConfig);
    }

    /**
     * Return an AmazonDynamoDB client, either from cache or newly-created
     * @param jaasOptions
     * @return com.amazonaws.services.dynamodbv2.AmazonDynamoDB
     */
    public AmazonDynamoDB createClient(final Map<String, ?> jaasOptions) {
        AmazonDynamoDB ddb = null;

        if (jaasOptions.containsKey(CACHE_ID)) {
            final String cacheId = (String) jaasOptions.get(CACHE_ID);
            synchronized (CACHE) {
                if (!CACHE.containsKey(cacheId)) {
                    ddb = createBuilder(jaasOptions).build();
                    CACHE.put(cacheId, ddb);
                    System.out.println("Created DynamoDB client");
                } else {
                    ddb = CACHE.get(cacheId);
                    System.out.println("Retrieved DynamoDB client from CACHE");
                }
            }
        } else {
            ddb = createBuilder(jaasOptions).build();
            System.out.println("Created DynamoDB client w/o cache");
        }
        return ddb;
    }
}