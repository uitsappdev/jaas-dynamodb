package edu.arizona.uits.sia.jaas.dynamodb;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class RunnableJaasTest implements Runnable {
    private Thread t;
    private String threadName;
    private List<List<String>> users;
    private int numIterations;
    private Random randomGenerator;

    RunnableJaasTest(String name, List<List<String>> users, int numIterations) {
        threadName = name;
        this.users = users;
        this.numIterations = numIterations;
        randomGenerator = new Random();
        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);
        for (int i = 0; i < numIterations; i++) {
            long startTime = 0;
            long stopTime = 0;
            long elapsedTime = 0;
            int idx = randomGenerator.nextInt(users.size());
            List user = users.get(idx);
            try {
                startTime = System.currentTimeMillis();
                LoginContext lc = new LoginContext("Test", new TestCallbackHandler((String) user.get(0), (String) user.get(1)));
                lc.login();
                stopTime = System.currentTimeMillis();

            } catch (LoginException e) {
                e.printStackTrace();
            }
            elapsedTime = (stopTime - startTime);
            System.out.println("Exec time: " + threadName + " = " + Long.toString(elapsedTime));
            // Let the thread sleep for a while.
//                Thread.sleep(50);
        }

        System.out.println("Thread " + threadName + " exiting.");
    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class JaasAuthenticationTest {

    public static void main(String[] args) {

        String fileName = args[1];
        int numThreads = Integer.parseInt(args[2]);
        int numIterations = Integer.parseInt(args[3]);
        File file = new File(fileName);

        // this gives you a 2-dimensional array of strings
        List<List<String>> lines = new ArrayList<>();
        Scanner inputStream;

        try {
            inputStream = new Scanner(file);

            while (inputStream.hasNext()) {
                String line = inputStream.next();
                String[] values = line.split(",");
                // this adds the currently parsed line to the 2-dimensional string array
                lines.add(Arrays.asList(values));
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.setProperty("java.security.auth.login.config", "dynamodb.config");

        for (int i = 0; i < numThreads; i++) {
            new RunnableJaasTest("Thread-" + i, lines, numIterations).start();
        }
    }
}