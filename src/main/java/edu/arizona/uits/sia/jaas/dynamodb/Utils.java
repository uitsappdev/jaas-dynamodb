// $Id: Utils.java,v 1.5 2003/02/17 20:13:23 andy Exp $
package edu.arizona.uits.sia.jaas.dynamodb;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Utility methods for com.tagish.auth.*. All the methods in here are static
 * so Utils should never be instantiated.
 *
 * @author Andy Armstrong, <A HREF="mailto:andy@tagish.com">andy@tagish.com</A>
 * @version 1.0.3
 */
public class Utils {
    private static final String ALGORITHM = "PBKDF2WithHmacSHA256";
    private static final int KEY_LENGTH = 256;
    private static final int ITERATION_COUNT = 10000;

    /**
     * Can't make these: all the methods are static
     */
    private Utils() {
    }

    /**
     * Turn a byte array into a char array containing a printable
     * hex representation of the bytes. Each byte in the source array
     * contributes a pair of hex digits to the output array.
     *
     * @param src the source array
     * @return a char array containing a printable version of the source
     * data
     */
    private static char[] hexDump(byte src[]) {
        char buf[] = new char[src.length * 2];
        for (int b = 0; b < src.length; b++) {
            String byt = Integer.toHexString((int) src[b] & 0xFF);
            if (byt.length() < 2) {
                buf[b * 2 + 0] = '0';
                buf[b * 2 + 1] = byt.charAt(0);
            } else {
                buf[b * 2 + 0] = byt.charAt(0);
                buf[b * 2 + 1] = byt.charAt(1);
            }
        }
        return buf;
    }

    /**
     * Zero the contents of the specified array. Typically used to
     * erase temporary storage that has held plaintext passwords
     * so that we don't leave them lying around in memory.
     *
     * @param pwd the array to zero
     */
    public static void smudge(char pwd[]) {
        if (null != pwd) {
            for (int b = 0; b < pwd.length; b++) {
                pwd[b] = 0;
            }
        }
    }

    /**
     * Zero the contents of the specified array.
     *
     * @param pwd the array to zero
     */
    public static void smudge(byte pwd[]) {
        if (null != pwd) {
            for (int b = 0; b < pwd.length; b++) {
                pwd[b] = 0;
            }
        }
    }

    /**
     * Perform PBKDF2-HMAC-SHA-256 hashing on the supplied password and return a char array
     * containing the encrypted password as a printable hex string. The hash is
     * computed on the low 8 bits of each character.
     *
     * @param pwd The password to encrypt
     * @return a character array containing a 64 character long hex encoded
     * PBKDF2-HMAC-SHA-256 hash of the password
     */

    public static char[] cryptPassword(char[] password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
        PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATION_COUNT, KEY_LENGTH);
        byte[] passwordDigest = factory.generateSecret(spec).getEncoded();
        char crypt[] = hexDump(passwordDigest);
        smudge(passwordDigest);
        return crypt;
    }
}
