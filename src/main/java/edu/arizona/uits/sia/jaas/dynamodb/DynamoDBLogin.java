package edu.arizona.uits.sia.jaas.dynamodb;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class DynamoDBLogin extends SimpleLogin {

    protected String                partitionKey;
    protected String                sortKey;
    protected String                table;
    protected String                pwSalt;
    protected AmazonDynamoDB        ddb;

    protected synchronized Vector validateUser(String username, char password[]) throws LoginException {
        Vector p = null;
        boolean failed = false;

        try {
            // get hash of password for lookup
            char[] hashedPw = Utils.cryptPassword(password, pwSalt.getBytes());

            // build DynamoDB item lookup request with username and hashed password
            HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
            key.put(partitionKey, new AttributeValue().withS(username));
            key.put(sortKey, new AttributeValue().withS(String.valueOf(hashedPw)));

            GetItemRequest request = new GetItemRequest()
                    .withTableName(table)
                    .withKey(key)
                    .withProjectionExpression("lastAuth");

            // submit getItem request and check result
            GetItemResult result = ddb.getItem(request);
            Map items = result.getItem();
            if (items != null) {
                p = new Vector();
                p.add(new TypedPrincipal(username, TypedPrincipal.USER));

                // update lastAuth time
                long unixTime = new Date().getTime();
                Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
                expressionAttributeValues.put(":lastAuth", new AttributeValue().withN(Long.toString(unixTime)));
                UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                        .withTableName(table)
                        .withKey(key)
                        .withUpdateExpression("set lastAuth = :lastAuth")
                        .withExpressionAttributeValues(expressionAttributeValues);
                UpdateItemResult updateResult = ddb.updateItem(updateItemRequest);
            } else {
                failed = true;
            }
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw new LoginException("Error hashing password (" + e.getMessage() + ")");
        } catch (Exception e) {
            throw new LoginException("DynamoDB error (" + e.getMessage() + ")");
        }
        if (failed) {
          throw new FailedLoginException("authentication failed");
        }
        return p;
    }

    public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
        super.initialize(subject, callbackHandler, sharedState, options);

        // get properties needed for DynamoDB query
        partitionKey = getOption("partitionKey", null);
        if (partitionKey == null) throw new Error("No primary partition key specified for table (partitionKey=?)");
        table = getOption("table", null);
        if (table == null) throw new Error("No DynamoDB table specified (table=?)");
        pwSalt = getOption("pwSalt", null);
        if (pwSalt == null) throw new Error("No password salt specified (pwSalt=?");
        sortKey = getOption("sortKey", null);

        // retrieve DynamoDB client from factory
        ddb = new DynamoDBClientFactory().createClient(options);
    }
}
