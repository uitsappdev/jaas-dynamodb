# jaas-dynamodb-shib

## A Shibboleth JAAS authentication module using AWS DynamoDB as a backend

### Installation

1. Install [Gradle](https://gradle.org/) and JDK 8, if not already installed.
2. Run `gradle shadowJar` in top level of project folder.
3. Copy `build/libs/jaas-dynamodb-all-shaded.jar` to your Shib IdP installation's `webapp/WEB-INF/lib` directory.

### Usage

example of usage of the module is in dynamodb.config

Usage of DynamoDBLogin, parameters:

- `region` = AWS region (e.g. "us-west-2")
- `accessKeyId` (optional) = AWS Access Key ID to use for IAM permissions; if this and `secretAccessKey` are not set, permissions will be inherited from the default credentials provider (e.g. provided via environment or IAM role)
- `secretAccessKey` (optional) = AWS Secret Access Key to use for IAM permissions; ff this and `accessKeyId` are not set, permissions will be inherited from the default credentials provider (e.g. provided via environment or IAM role)
- `table` = name of DynamoDB table to access for credentials
- `partitionKey` = DynamoDB partition (hash) key to use for GetItem calls
- `sortKey` = DynamoDB sort key to use for GetItem calls
- `pwSalt` = salt to use with PBKDF2 function to hash supplied password for comparison
- `cacheId`(optional) = unique cache identifier which will be associated with a persistent DynamoDB client pool; if this is not supplied a new DynamoDB client pool will be created for each invocation

### DynamoDB table definition

The DynamoDB table requires 2 fields:

- <partitionKey>
    - type = 'String'
    - this field holds the hashed password value, which must be hashed using PBKDF2 w/ SHA256, using the salt specified by `pwSalt`
- <sortKey>
    - type = 'String'
    - this field holds the username

When the `validateUser()` method in `DynamoDBLogin` successfully authenticates a credential, it sets (or updates) a field named `lastAuth` with the epoch time (in milliseconds) of the authentication event.  
